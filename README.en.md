# MindVision

## Introduction

MindVision is an open source semantic segmentation toolbox based on MindSpore.

The master branch works with **MindSPore 1.2**.


## License

This project is released under the [Apache 2.0 license](LICENSE).

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mind_spore/mindspore-segmentation/issues).

## Acknowledgement

MindSpor is an open source project that welcome any contribution and feedback.
We wish that the toolbox and benchmark could serve the growing research
community by providing a flexible as well as standardized toolkit to reimplement existing methods
and develop their own new semantic segmentation methods.

## Citation

If you find this project useful in your research, please consider citing:

```latex
@misc{mindvsion2021,
    title={{MindVision}:MindSpore Vision Toolbox and Benchmark},
    author={MindVision Contributors},
    howpublished = {\url{https://gitee.com/mindspore/mindvision}},
    year={2021}
}
```