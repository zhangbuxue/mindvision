# mindvision-pose

## Introduction

MindVision-Pose is an open-source toolbox for pose estimation based on MindSpore. It is a part of the MindVision project.

The master branch develop on **MindSPore 1.2**.

![demo image](docs/demo1.png)


## License

This project is released under the [Apache 2.0 license](LICENSE).

## Benchmark and model zoo

Results and models are available in the [model zoo](docs/model_zoo.md).

Supporting backbones:

- [x] [AlexNet]
- [x] [CPM]
- [x] [Hourglass]
- [x] [HRNet]
- [x] [MobilenetV2]
- [x] [ResNet]
- [x] [ResNetV1D]
- [x] [ResNext]
- [x] [SCNet]
- [x] [SEResNet]
- [x] [ShufflenetV1]

Supported methods for human pose estimation:

- [x] [CPM]
- [x] [SimpleBaseline]
- [x] [HRNet]
- [x] [Hourglass]
- [x] [SCNet]
- [x] [HigherHRNet]
- [x] [DarkPose]

Supported datasets:

- [x] [COCO](http://cocodataset.org/)
- [x] [MPII](http://human-pose.mpi-inf.mpg.de/)
- [x] [MPII-TRB](https://github.com/kennymckormick/Triplet-Representation-of-human-Body)
- [x] [AI Challenger](https://github.com/AIChallenger/AI_Challenger_2017)
- [x] [OCHuman](https://github.com/liruilong940607/OCHumanApi)
- [x] [CrowdPose](https://github.com/Jeff-sjtu/CrowdPose)
- [x] [OneHand10K](https://www.yangangwang.com/papers/WANG-MCC-2018-10.html)

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mindspore/mindvision/issues).

## Contributing

We appreciate all contributions to improve MindSpore Segmentation. Please refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for the contributing guideline.

## Contributors

ZOMI(chenzomi12@gmail.com)
xiexingwang(1570475816@qq.com)
weihao(1920495491@qq.com)
hu-zhaoqiang(2467868073@qq.com)
OwenLee666(51775540@qq.com)
hansaifei(453751375@qq.com)
ljj(1309968475@qq.com)
chen-chujia(2350422432@qq.com)
jianxu(864753683@qq.com)
zhong-shanji(935494217@qq.com)
HandSomEiM(houshuai319@gmail.com)
li-ying_a(1026312089@qq.com)
huangweijun(gdut_hwj_1@163.com)
CoDoJD(1685497601@qq.com)
lisa(xinyi.lisa.chen@gmail.com)
xudongliang(939369646@qq.com)
chen jiayang(744147327@qq.com)
cenfeijing（1521313560@qq.com)
lidong(lidon6@163.com)

## Acknowledgement

MindVision is an open source project that welcome any contribution and feedback. We wish that the toolbox and benchmark could serve the growing research community by providing a flexible as well as standardized toolkit to reimplement existing methods and develop their own new pose estimation methods.
